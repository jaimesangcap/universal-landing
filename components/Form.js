import styled from "styled-components";

const formBaseStyles = props => `
  height: 32px;
  padding: 6px 10px;
  background-color: #fff;
  border: 1px solid #d1d1d1;
  border-radius: 4px;
  box-shadow: none;
  box-sizing: border-box;
  &:focus {
    border: 1px solid #33c3f0;
    outline: 0;
  }
`;

export const Input = props => (
  <div>
    <input type="text" {...props} />
    <style jsx>{`
      input {
        ${formBaseStyles(props)};
      }
    `}</style>
  </div>
);

export const TextArea = styled.textarea`
  ${props => formBaseStyles(props)};
  min-height: 65px;
  padding-top: 6px;
  padding-bottom: 6px;
`;
