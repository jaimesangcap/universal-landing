import React from "react";
import { H4 } from "./H4";

export default props => (
  <div>
    <section>
      <H4>Airbnb</H4>
      <ul>
        <li>
          <a href="#">Careers</a>
        </li>
        <li>
          <a href="#">Press</a>
        </li>
        <li>
          <a href="#">Policies</a>
        </li>
      </ul>
    </section>
    <style jsx>{`
      li {
        color: #e5e5e5;
      }
    `}</style>
  </div>
);
