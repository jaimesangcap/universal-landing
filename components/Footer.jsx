import React from "react";

import H4 from "./H4";

export default props => (
  <div>
    <footer>
      <section>
        <H4>Airbnb</H4>
        <ul>
          <li>
            <a href="#">Careers</a>
          </li>
          <li>
            <a href="#">Press</a>
          </li>
          <li>
            <a href="#">Policies</a>
          </li>
        </ul>
      </section>
    </footer>
    <style jsx>{`
      footer {
        margin-top: 5rem;
      }

      ul {
        margin: 0;
        padding: 0;
        margin-top: 16px;
      }

      li {
        list-style-type: none;
        margin-bottom: 4px;
      }

      a {
        text-decoration: none;
        color: #767676;
        font-size: 14px;
      }
    `}</style>
  </div>
);
