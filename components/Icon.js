import React from "react";

export default ({ user, logo, clock, ...props }) => (
  <svg className="icon" {...props}>
    {user && (
      <path
        d="M12,15 L0,15 L0.01,14.11 C0.11,11.46 1.983,9.39 4.273,9.39 L7.733\
 ,9.39 C10.086,..."
      />
    )}
    {clock && (
      <path
        d="M10.0001,8 L9.0001,8 L9.0001,6 C9.0001,5.448 8.5531,5 8.0001,5 C7.447\
 1,5 7.0001,..."
      />
    )}
    {logo && (
      <g>
        <polygon
          id="Shape"
          points="152.9 0 193.25 69.95 152.9 139.85 72.15 139.85 61.7 121.75 131\
 .9 0"
        />
        <polygon id="Shape" points="111.9 0 51.7 104.45 31.8 69.95 72.15 0" />
      </g>
    )}
  </svg>
);
